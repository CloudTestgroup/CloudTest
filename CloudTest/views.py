#!/usr/bin/python
#-*-coding:utf-8-*-
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponse
import os
from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.template import RequestContext

'''
def index(request):
    return render(request, 'zzzz.html', )

def butt(request):
    test_file = request.FILES.get('file')
    if test_file.size < 20480000:  # 限制文件不能大于2M
        test_file.name = "test" + ".yml"  # 用“用户id.yml”作为文件名
    path = default_storage.save('static/yaml' + test_file.name, ContentFile(test_file.read()))  # 存储文件第一步，这里要设置文件在项目目录里的路径
    tmp_file = os.path.join(settings.MEDIA_ROOT, path)  # 存储文件第二步
'''

def upload(request):
    if request.method == "POST":
        obj = request.FILES['file']
        path = '/Users/zhuruian/new/CloudTest/static/yaml'
        f = open(os.path.join(path, obj.name), 'wb')
        for line in obj.chunks():
            f.write(line)
        f.close()
        return render_to_response('second_cloudTest.html') #HttpResponse('successfully')
    return render_to_response('zzzz.html')

def index(request):
    return render(request, 'second_cloudTest.html', )